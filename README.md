# Klondike

Play a solitaire with the feeling and simplicity of the very classic deck style.

No drag-and-drop
----------------

Play faster just clicking on the card or group of cards you want to move and then click on the
valid stack where the card(s) will be placed.

Important side note
-------------------

The source code has been built and structured using Maven following the MVC design pattern, so
to recompile it just use the `mvn compile` command or the `mvn package` to obtain the Jar file
directly.

Keys
---------
>>>
Press <kbd>N</kbd> to start a new game.

Press <kbd>U</kbd> to undo the last move.

Press <kbd>D</kbd> to choose between deck styles.

Press <kbd>S</kbd> to show game statistics.
>>>

Take a look
-----------
![img1](chunks/img1.png)
![img2](chunks/img2.png)
![img3](chunks/img3.png)
![img4](chunks/img4.png)
